Under development
-----------------------------------------------

1.2.0 2023-05-24
-----------------------------------------------
- Feature: Add mobilephone number generator (Kalmer)

1.1.0 2023-05-10
-----------------------------------------------
- Bugfix: Fix phone number formats (Kalmer)

1.0.0 2023-05-04
-----------------------------------------------
- Feature: Initial implementation (Kalmer)
