<?php

namespace Faker\Test\Lithuania;

use Faker\Generator;
use Faker\Lithuania\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /**
     * @var Generator
     */
    private $_faker;

    protected function setUp(): void
    {
        $faker = new Generator();
        $faker->addProvider(new Address($faker));
        $this->_faker = $faker;
    }

    /**
     * Test the validity of municipality
     */
    public function testMunicipality()
    {
        $municipality = $this->_faker->municipality();
        $this->assertNotEmpty($municipality);
        $this->assertStringEndsWith('savivaldybė', $municipality);
    }
}
